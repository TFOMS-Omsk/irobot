﻿namespace iRobot
{
    public class ArgumentPath
    {
        private static ArgumentPath _argument;

        public string Path { get; set; }

        public static ArgumentPath Instance => _argument ?? (_argument = new ArgumentPath());
    }
}