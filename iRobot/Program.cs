﻿using System.CommandLine;
using System.ServiceProcess;

namespace iRobot
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        static void Main(string[] args)
        {
            string configPath = string.Empty;
            ArgumentSyntax.Parse(args, syntax =>
            {
                syntax.DefineOption("p|path", ref configPath, "path file config");
            });
            
            ArgumentPath.Instance.Path = configPath;

            var servicesToRun = new ServiceBase[]
            {
                new OmsService()
            };

            ServiceBase.Run(servicesToRun);
        }
    }
}