﻿using System;
using System.ServiceProcess;
using Lib.Worker;

namespace iRobot
{
    public partial class OmsService : ServiceBase
    {
        private WorkerJob _job;

        public OmsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (string.IsNullOrWhiteSpace(ArgumentPath.Instance.Path))
            {
                throw new ArgumentNullException(nameof(ArgumentPath.Instance.Path), "Ошибка аргумента задания пути файла конфигурации");
            }

            _job = new WorkerJob(ArgumentPath.Instance.Path);
            _job.Start();
        }

        protected override void OnStop()
        {
            _job?.Dispose();
        }

        protected override void OnShutdown()
        {
            _job?.Dispose();
        }
    }
}
