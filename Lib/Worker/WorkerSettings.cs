﻿namespace Lib.Worker
{
    public class DataInstances
    {
        public string Erz { get; set; }

        public string Sprav { get; set; }

        public string Tarif { get; set; }
    }

    public class Paths
    {
        public string Input { get; set; }

        public string InputPath => Input.Replace("UNC", "\\");

        public string Output { get; set; }

        public string Extract { get; set; }

        public string SegmentInput { get; set; }

        public string SegmentOutput { get; set; }

        public string HandBookSmo { get; set; }
    }

    public class WorkerSettings
    {
        public DataInstances DataInstances { get; set; }

        public Paths Paths { get; set; }

        public int TimeoutCentralSegment { get; set; }

        /// <summary>
        /// Медицинские организации исключения
        /// </summary>
        public string[] MedException { get; set; }
    }

    public class RobotConfig
    {
        private static RobotConfig _config;

        private static readonly object Obj = new object();

        public WorkerSettings Settings { get; set; }

        public static RobotConfig Current
        {
            get
            {
                lock (Obj)
                {
                    if (_config == null)
                    {
                        _config = new RobotConfig();
                    }
                }

                return _config;
            }
        }
    }
}