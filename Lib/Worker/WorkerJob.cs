﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.IO.FileSystem;
using System.Threading;
using System.Threading.Tasks.Channels;
using Lib.Ioc;
using Lib.Worker.Job;
using Lib.Worker.Model;
using Polly;
using System.Linq;
using System.Text;

namespace Lib.Worker
{
    public class WorkerJob : IDisposable
    {
        private IJobLog Logger => IocContainer.Default.Resolve<IJobLog>();

        private readonly string _path;

        private PollingWatcher _watcherInput;

        private PollingWatcher _watcherSegment;

        /// <summary>
        /// Канал для обработки файлов
        /// </summary>
        private readonly Channel<FileChange> _fileChannel;

        /// <summary>
        /// Канал для обработки файлов с ЦС
        /// </summary>
        private readonly Channel<FileChange> _fileSegment;

        /// <summary>
        /// Канал упаковки в архивы
        /// </summary>
        private readonly Channel<JobMessage> _filePack;

        private readonly ConcurrentDictionary<string, JobMessage> _dictionaryPerson = new ConcurrentDictionary<string, JobMessage>();

        /// <summary>
        /// Поток для обработки поступающих файлов в каталог
        /// </summary>
        private Thread _threadInput;

        /// <summary>
        /// Поток обработки файлов из центрального сегмета
        /// </summary>
        private Thread _threadSegment;

        /// <summary>
        /// Поток упаковки файлов
        /// </summary>
        private Thread _threadPack;

        /// <summary>
        /// Обработка файлов ожидания
        /// </summary>
        private readonly Timer _timer;

        public WorkerJob(string path)
        {
            _path = path;

            IocContainer.Default.Single<IConfig, Config>()
                .PerRequest<IFileZip, FileZip>()
                .PerRequest<IPackXml, PackXml>()
                .PerRequest<IErzJob, ErzJob>()
                .PerRequest<ICentralSegment, CentralSegment>()
                .PerRequest<IArhivePack, ArchivePack>()
                .Single<IHandBook, HandBook>()
                .Single<ITarifUsl, TarifUsl>()
                .Single<IJobLog, JobLog>();

            _fileChannel = Channel.CreateBounded<FileChange>(50);
            _fileSegment = Channel.CreateBounded<FileChange>(50);
            _filePack = Channel.CreateBounded<JobMessage>(30);

            _timer = new Timer(OnTimerTick, null, 1000, Timeout.Infinite);
        }

        private async void OnTimerTick(object state)
        {
            int timeout = RobotConfig.Current.Settings.TimeoutCentralSegment;
            foreach (var element in _dictionaryPerson.Values.Where(p => p.Started > p.Started.AddMinutes(timeout)))
            {
                JobMessage job;
                if (_dictionaryPerson.TryRemove(element.State.MessageId, out job))
                {
                    CreateArchiveError("Центральный сегмент не доступен.", element.Name);
                    await _filePack.Out.WriteAsync(job);
                }
            }

            _timer.Change(1000, Timeout.Infinite);
        }

        public async void Start()
        {
            //Обрабатываем файлы с конфигом
            var config = IocContainer.Default.Resolve<IConfig>();
            WorkerSettings settings = await config.GetSettings(_path);

            if (settings == null)
            {
                return;    
            }

            RobotConfig.Current.Settings = settings;

            //создаем каталоги
            if (!Directory.Exists(settings.Paths.Input))
            {
                Directory.CreateDirectory(settings.Paths.Input);
            }

            if (!Directory.Exists(settings.Paths.Output))
            {
                Directory.CreateDirectory(settings.Paths.Output);
            }

            if (!Directory.Exists(settings.Paths.Extract))
            {
                Directory.CreateDirectory(settings.Paths.Extract);
            }

            //Запускаем потоки
            _threadInput = new Thread(InputWorker);
            _threadInput.Start();

            _threadSegment = new Thread(InputSegment);
            _threadSegment.Start();

            _threadPack = new Thread(ThreadXmlPack);
            _threadPack.Start();

            WorkJobChanged(settings);
        }
        
        /// <summary>
        /// Запуск сервисов на отслеживание
        /// </summary>
        private void WorkJobChanged(WorkerSettings settings)
        {
            _watcherInput = new PollingWatcher(settings.Paths.Input)
            {
                ChangedDetailed = ChangedInput
            };

            _watcherSegment = new PollingWatcher(settings.Paths.SegmentOutput)
            {
                ChangedDetailed = ChangedSegment
            };

            _watcherSegment.AddExtension("uprak2");

            _watcherInput.Start();
            _watcherSegment.Start();
        }

        /// <summary>
        /// Отслеживание рабочего каталога
        /// </summary>
        /// <param name="files"></param>
        private async void ChangedInput(FileChange[] files)
        {
            foreach (var file in files.Where(p => p.ChangeType != ChangeType.Deleted))
            {
                await _fileChannel.Out.WriteAsync(file);
            }
        }

        /// <summary>
        /// Отслеживание папки ццентрального сегмента
        /// </summary>
        /// <param name="files"></param>
        private async void ChangedSegment(FileChange[] files)
        {
            foreach (var file in files.Where(p => p.ChangeType != ChangeType.Deleted))
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.Name);
                if (fileNameWithoutExtension != null)
                {
                    string key = fileNameWithoutExtension.Substring(6);
                    if (_dictionaryPerson.ContainsKey(key))
                    {
                        await _fileSegment.Out.WriteAsync(file);
                    }
                }
            }
        }

        private async void InputWorker()
        {
            //Получаем файлы из канала начинаем обрабатывать их
            while (await _fileChannel.In.WaitToReadAsync())
            {
                try
                {
                    var file = await _fileChannel.In.ReadAsync();
                    string name = Path.GetFileNameWithoutExtension(file.Name);

                    var unpack = Policy
                        .Handle<Exception>()
                        .Retry(3).ExecuteAndCapture(() =>
                        {
                            //Распаковываем архив
                            IFileZip zipArchive = IocContainer.Default.Resolve<IFileZip>();
                            var state = zipArchive.Unpack(file);
                            return state;
                        });

                    //Обработка ошибок для распаковки файлов
                    if (unpack.Outcome == OutcomeType.Failure)
                    {
                        CreateArchiveError("Ошибка распаковки файла", name);

                        Logger.WriteLog($"Ошибка распаковки файла {file.Path}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{unpack.FinalException.Message} {file.Path}", EventLogEntryType.Warning);
                        continue;
                    }

                    if (unpack.Outcome == OutcomeType.Successful)
                    {
                        Logger.WriteLog($"Файл распакован {file.Path}");
                    }

                    var packXml = await Policy.Handle<Exception>()
                        .RetryAsync(3).ExecuteAndCaptureAsync(() =>
                        {
                            IPackXml xml = IocContainer.Default.Resolve<IPackXml>();
                            return xml.GetPersons(unpack.Result);
                        });

                    //Обработка ошибок для разбора xml
                    if (packXml.Outcome == OutcomeType.Failure)
                    {
                        CreateArchiveError("Ошибка при разборе xml файла", name);

                        Logger.WriteLog($"Ошибка парсинга файла {file.Path}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{packXml.FinalException.Message} {file.Path}", EventLogEntryType.Warning);
                        continue;
                    }

                    if (packXml.Outcome == OutcomeType.Successful)
                    {
                        Logger.WriteLog($"Синтаксический анализ xml для файла {file.Path}");
                    }

                    var erzJob = await Policy.Handle<Exception>()
                        .RetryAsync(3)
                        .ExecuteAndCaptureAsync(() =>
                        {
                            IErzJob job = IocContainer.Default.Resolve<IErzJob>();
                            return job.GetErzList(packXml.Result);
                        });

                    //Обработка ошибок при поиске в Erz
                    if (erzJob.Outcome == OutcomeType.Failure)
                    {
                        CreateArchiveError("Ошибка при поиске в региональном регистре застрахованных", name);

                        Logger.WriteLog($"Ошибка поиска в ERZ для файла {file.Path}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{erzJob.FinalException.Message} {file.Path}", EventLogEntryType.Warning);
                        continue;
                    }

                    if (erzJob.Outcome == OutcomeType.Successful)
                    {
                        Logger.WriteLog($"Поиск в региональном регистре застрахованных для файла {file.Path}");
                    }

                    var segmentJob = await Policy.Handle<Exception>()
                        .RetryAsync(3).ExecuteAndCaptureAsync(() =>
                        {
                            ICentralSegment job = IocContainer.Default.Resolve<ICentralSegment>();
                            return job.MessageId(unpack.Result.MessageId)
                                    .Persons(erzJob.Result.Where(p => p.PersonType == PersonSearchType.None).ToList())
                                    .ShapeBuildAsync();
                        });

                    //Ошибки при отправке данных в центральный сегмент
                    if (segmentJob.Outcome == OutcomeType.Failure)
                    {
                        CreateArchiveError("Ошибка при отправке данных в ЦС сегмент", name);

                        Logger.WriteLog($"Ошибка при отправке в ЦС для файла {file.Path}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{segmentJob.FinalException.Message} {file.Path}", EventLogEntryType.Warning);

                        continue;
                    }

                    if (segmentJob.Outcome == OutcomeType.Successful)
                    {
                        _dictionaryPerson.TryAdd(unpack.Result.MessageId, 
                            new JobMessage(erzJob.Result.ToList(), name, unpack.Result));

                        Logger.WriteLog($"Отправка данных в центральный сегмент для файла {file.Path}");
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private async void InputSegment()
        {
            while (await _fileSegment.In.WaitToReadAsync())
            {
                try
                {
                    var file = await _fileSegment.In.ReadAsync();
                    var unpack = Policy
                        .Handle<Exception>()
                        .Retry(3).ExecuteAndCapture(() =>
                        {
                            ICentralSegment segment = IocContainer.Default.Resolve<ICentralSegment>();
                            return segment.ReadPack("\\" + file.Path.Replace("UNC", string.Empty)).ReadMessageAsync();
                        });

                    var key = Path.GetFileNameWithoutExtension(file.Name);

                    //Обработка ошибок для сообщений цс
                    if (unpack.Outcome == OutcomeType.Failure)
                    {
                        
                        JobMessage removeMessage;
                        if (_dictionaryPerson.TryRemove(key?.Substring(6) ?? string.Empty, out removeMessage))
                        {
                            CreateArchiveError("Ошибка при получении данных из ЦС", Path.GetFileNameWithoutExtension(removeMessage.Name));
                        }
                        
                        Logger.WriteLog($"Ошибка при получении данных из ЦС для файла {file.Path}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{unpack.FinalException.Message} {file.Path}", EventLogEntryType.Warning);

                        continue;
                    }

                    if (unpack.Outcome == OutcomeType.Successful)
                    {
                        Logger.WriteLog($"Обработка ответа из ЦС для файла {file.Path}");
                    }

                    var persons = LinkCentralSegment(key?.Substring(6), unpack.Result.Result);

                    JobMessage message;
                    if (_dictionaryPerson.TryRemove(key?.Substring(6) ?? string.Empty, out message))
                    {
                        await _filePack.Out.WriteAsync(new JobMessage(persons.ToList(), message.Name, message.State));
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private IEnumerable<Person> LinkCentralSegment(string key, IEnumerable<InsuranceGroupEnp> messages)
        {
            JobMessage message;
            if (_dictionaryPerson.TryGetValue(key, out message))
            {
                var personDictionary = message.Persons.ToDictionary(p => p.Id);
                foreach (var item in messages)
                {
                    Person person;
                    if (personDictionary.TryGetValue(item.Number, out person))
                    {
                        if (person.ErzResult == null)
                        {
                            person.ErzResult = new ErzResult();
                        }

                        person.ErzResult.Enp = item.ItemsEnp.FirstOrDefault();
                        
                        //Выбираем полис
                        var polis = item.Items.FirstOrDefault(p => string.IsNullOrWhiteSpace(p.DateOut)) ??
                            item.Items.FirstOrDefault(p => p.DateIn.AsDateTime() <= person.DateOut && p.DateOut.AsDateTime() >= person.DateOut);
                        if (polis != null)
                        {
                            if (!string.IsNullOrWhiteSpace(polis.CodeOkato) && polis.CodeOkato.Trim().Contains("52000"))
                            {
                                person.PersonType = PersonSearchType.None;
                            }
                            else
                            {
                                person.PersonType = PersonSearchType.Segment;
                            }
                            
                            person.ErzResult.Vpolic = polis.TypePolis;
                            person.ErzResult.Ogrn = polis.Ogrn;
                            person.ErzResult.Number = new[] { "С", "В" }.Contains(polis.TypePolis) ? polis.Polis : person.ErzResult.Enp;
                            person.ErzResult.Okato = polis.CodeOkato;

                            personDictionary[item.Number] = person;
                        }
                    }
                }

                return personDictionary.Select(p => p.Value);
            }

            return Enumerable.Empty<Person>();
        }

        private async void ThreadXmlPack()
        {
            while (await _filePack.In.WaitToReadAsync())
            {
                try
                {
                    var job = await _filePack.In.ReadAsync();
                    var arhivePack = await Policy.Handle<Exception>()
                        .RetryAsync(3)
                        .ExecuteAndCaptureAsync(() =>
                        {
                            IArhivePack pack = IocContainer.Default.Resolve<IArhivePack>();
                            return pack.AddJob(job).Build();
                        });
                    
                    //Обработка ошибок упаковки файлов
                    if (arhivePack.Outcome == OutcomeType.Failure)
                    {
                        CreateArchiveError("Ошибка при формировании файлов", Path.GetFileNameWithoutExtension(job.Name));

                        Logger.WriteLog($"Ошибка при формировании файлов для файла {job.Name}", EventLogEntryType.Warning);
                        Logger.WriteLog($"{arhivePack.FinalException.Message} {job.Name}", EventLogEntryType.Warning);
                    }

                    if (arhivePack.Outcome == OutcomeType.Successful)
                    {
                        Logger.WriteLog($"Файлы сформированны для файла {job.Name}");
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public void Dispose()
        {
            _watcherInput?.Dispose();
            _watcherSegment?.Dispose();

            _threadInput.Abort();
            _threadPack.Abort();
            _threadSegment.Abort();

            _dictionaryPerson.Clear();

            _timer.Dispose();
        }

        private async void CreateArchiveError(string message, string file)
        {
            var output = RobotConfig.Current.Settings.Paths.Output;

            string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string directory = Path.Combine(dir, Guid.NewGuid().ToString());

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using (var streamWriter = new StreamWriter(new FileStream(Path.Combine(directory, "error.txt"), FileMode.CreateNew), Encoding.GetEncoding(1251)))
            {
                await streamWriter.WriteAsync(message);
            }
            
            int count = Directory.GetFiles(output).Count(p => Path.GetFileNameWithoutExtension(p)?.Contains(file) ?? false);
            if (count > 0)
            {
                file += $"({count})err.zip";
            }
            else
            {
                file += ".zip";
            }

            ZipFile.CreateFromDirectory(directory, Path.Combine(output, file));

            foreach (var item in Directory.GetFiles(directory))
            {
                File.Delete(item);
            }

            Directory.Delete(directory);
        }
    }
}