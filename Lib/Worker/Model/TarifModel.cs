﻿using System;

namespace Lib.Worker.Model
{
    public class Tarif
    {
        public int IdCodeUsl { get; set; }

        public double TarifSum { get; set; }

        public DateTime DateBegin { get; set; }
    }

    public class SprCodeUsl
    {
        public int Id { get; set; }

        public string CodeUsl { get; set; }
    }

    public class TarifModel
    {
        public double Tarif { get; set; }

        public DateTime DateBegin { get; set; }

        public string CodeUsl { get; set; }
    }
}