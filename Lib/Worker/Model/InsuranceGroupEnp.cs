﻿using System.Collections.Generic;

namespace Lib.Worker.Model
{
    public class InsuranceGroupEnp
    {
        public List<Insurance> Items { get; private set; }

        public List<string> ItemsEnp { get; private set; }

        public int Number { get; private set; }

        public InsuranceGroupEnp(List<Insurance> items, List<string> enp, int number)
        {
            Items = items;
            ItemsEnp = enp;
            Number = number;
        }
    }
}