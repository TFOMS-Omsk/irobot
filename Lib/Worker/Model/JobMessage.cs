﻿using System;
using System.Collections.Generic;
using Lib.Worker.Job;

namespace Lib.Worker.Model
{
    public class JobMessage
    {
        public List<Person> Persons { get; }

        public DateTime Started { get; }

        public string Name { get; }

        public MessageState State { get; }

        public JobMessage(List<Person> persons, string name, MessageState state)
        {
            Persons = persons;
            Started = DateTime.Now;
            Name = name;
            State = state;
        }
    }
}