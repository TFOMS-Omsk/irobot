﻿namespace Lib.Worker.Model
{
    public class Insurance
    {
        public string Num { get; set; }

        public string DateIn { get; set; }

        public string DateOut { get; set; }

        public string CodeOkato { get; set; }

        public string TypePolis { get; set; }

        public string Polis { get; set; }

        public string Ogrn { get; set; }
    }
}