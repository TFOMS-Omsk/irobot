﻿using System;

namespace Lib.Worker.Model
{
    public enum PersonSearchType
    {
        None,
        Segment,
        Erz
    }

    public class Person
    {
        public int Id { get; set; }

        public string IdPac { get; set; }

        public string Fam { get; set; }

        public string Im { get; set; }

        public string Ot { get; set; }

        public string W { get; set; }

        public DateTime Dr { get; set; }

        public string Doctype { get; set; }

        public string DocSer { get; set; }

        public string DocNum { get; set; }

        public string Snils { get; set; }

        public DateTime DateOut { get; set; }

        public PersonSearchType PersonType { get; set; } = PersonSearchType.None;

        public ErzResult ErzResult { get; set; }
    }
}