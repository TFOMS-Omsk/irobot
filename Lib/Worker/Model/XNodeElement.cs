﻿using System.Xml.Linq;

namespace Lib.Worker.Model
{
    public class XNodeElement
    {
        public XElement Element { get; private set; }

        /// <summary>
        /// Свойство прочитанного документа
        /// </summary>
        public bool IsRead { get; set; }

        public XNodeElement(XElement element)
        {
            Element = element;
        }
    }
}