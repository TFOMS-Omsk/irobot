﻿using System;

namespace Lib.Worker.Model
{
    public class ErzResult
    {
        public int Id { get; set; }

        public string Im { get; set; }

        public string Fam { get; set; }

        public string Ot { get; set; }

        public DateTime Dr { get; set; }

        public string Enp { get; set; }

        public int IdPol { get; set; }

        public int SkDog { get; set; }

        /// <summary>
        /// nsdoc + sdoc
        /// </summary>
        public string Document { get; set; }

        public string Ndoc { get; set; }

        public string Polblank { get; set; }

        public int IdDoct { get; set; }

        public string Seria { get; set; }

        public string Number { get; set; }

        public string Vpolic { get; set; }

        public string Ogrn { get; set; }

        public string Okato { get; set; }
    }
}