﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class HandBook : IHandBook
    {
        public async Task<IEnumerable<SmoHandBook>> GetBookAsync()
        {
            using (var reader = new StreamReader(RobotConfig.Current.Settings.Paths.HandBookSmo))
            {
                var result = await reader.ReadToEndAsync();
                return from element in XDocument.Parse(result).Elements("packet").Elements("insCompany")
                       select new SmoHandBook
                       {
                           SmoCode = element.Element("smocod")?.Value ?? string.Empty,
                           Ogrn = element.Element("Ogrn")?.Value ?? string.Empty,
                           Okato = element.Element("tf_okato")?.Value ?? string.Empty
                       };
            }
        }
    }
}