﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public interface IPackXml
    {
        Task<IEnumerable<Person>> GetPersons(MessageState state);
    }
}