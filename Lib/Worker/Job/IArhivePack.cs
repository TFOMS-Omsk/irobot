﻿using System.Threading.Tasks;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public interface IArhivePack
    {
        IArhivePack AddJob(JobMessage job);

        Task Build();
    }
}