﻿using System.Threading.Tasks;

namespace Lib.Worker.Job
{
    public interface IConfig
    {
        Task<WorkerSettings> GetSettings(string path);
    }
}