﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class ErzJob : IErzJob
    {
        private readonly SqlConnection _sqlConnection;
        
        private readonly CancellationToken _cancellationToken = new CancellationToken();

        private DataInstances Instances => RobotConfig.Current.Settings.DataInstances;

        public ErzJob()
        {
            _sqlConnection = new SqlConnection(Instances.Erz);
        }

        public Task<IEnumerable<Person>> GetErzList(IEnumerable<Person> persons)
        {
            return Search(persons);
        }

        private async Task<IEnumerable<Person>> Search(IEnumerable<Person> persons)
        {
            await _sqlConnection.OpenAsync(_cancellationToken);
            string createTable = "create table #irobot(code Int, fam Varchar(500), im Varchar(500), ot Varchar(500), dr DateTime, doctype Varchar(500), " +
                                 "docser Varchar(500), docnum Varchar(500), snils Varchar(500), dateout DateTime);";

            await _sqlConnection.ExecuteAsync(createTable);

            var sb = new StringBuilder();
            int index = 0;
            sb.AppendLine("SET DATEFORMAT YMD;");

            var personItems = persons as Person[] ?? persons.ToArray();
            foreach (var item in personItems)
            {
                string dr = $"{item.Dr.Year}-{item.Dr.Month.AsNum()}-{item.Dr.Day.AsNum()}";
                string dateOut = $"{item.DateOut.Year.AsNum()}-{item.DateOut.Month.AsNum()}-{item.DateOut.Day.AsNum()}";

                sb.AppendLine("insert into #irobot(code, fam, im, ot, dr, docser, docnum, snils, dateout) " +
                              $"values({item.Id}, \'{item.Fam}\', \'{item.Im}\', \'{item.Ot}\', \'{dr}\', \'{item.DocSer}\', \'{item.DocNum}\', \'{item.Snils}\', \'{dateOut}\')");

                if (index == 1000)
                {
                    await _sqlConnection.ExecuteAsync(sb.ToString(), commandTimeout: 0);

                    index = 0;
                    sb.Clear();

                    sb.AppendLine("SET DATEFORMAT YMD;");
                }
            }

            await _sqlConnection.ExecuteAsync(sb.ToString(), commandTimeout: 0);

            string sql = "SELECT rob.code AS Id, b.name AS Im, b.family AS Fam, b.father AS Ot, b.vozrast AS Dr, b.enp AS Enp, b.id_pol AS IdPol, " +
                         "b.skdog1 AS SkDog, RTRIM(LTRIM(b.nsdoc)) + RTRIM(LTRIM(b.sdoc)) AS Document, " +
                         "b.ndoc AS Ndoc, b.polblank AS Polblank, b.id_doc AS IdDoc, " +
                         "b.seria AS Seria, b.number AS Number, b.vpolic AS Vpolic FROM #irobot rob " +
                         "JOIN(SELECT ph.ID, ph.vozrast, sf.family, sn.name, so.father, ph.enp, " +
                         "ph.id_pol, ph.skdog1, ph.nsdoc, ph.sdoc, ph.ndoc, " +
                         "ph.polblank, ph.id_doc, ph.seria, ph.number, ph.vpolic FROM polises_history ph " +
                         "JOIN spr_fam sf ON ph.id_fam = sf.id_fam " +
                         "JOIN spr_nam sn ON ph.id_nam = sn.id_nam " +
                         "JOIN spr_otc so ON ph.id_otc = so.id_otc " +
                         "WHERE ph.id_sost IN(164, 512)) AS b " +
                         "ON YEAR(b.vozrast) = YEAR(rob.dr) AND " +
                         "MONTH(b.vozrast) = MONTH(rob.dr) AND " +
                         "DAY(b.vozrast) = DAY(rob.dr) AND " +
                         "REPLACE(RTRIM(LTRIM(b.name)), 'Ё', 'Е') = RTRIM(LTRIM(rob.im)) AND " +
                         "REPLACE(RTRIM(LTRIM(b.family)), 'Ё', 'Е') = RTRIM(LTRIM(rob.fam)) AND " +
                         "REPLACE(RTRIM(LTRIM(b.father)), 'Ё', 'Е') = RTRIM(LTRIM(rob.ot)) " +
                         "WHERE b.ID IN(SELECT * FROM dbo.Get_polises_from_history(rob.dateout) gpfh) " +
                         "SELECT rob.code AS Id, b.name AS Im, b.family AS Fam, b.father AS Ot, b.vozrast AS Dr, b.enp AS Enp, b.id_pol AS IdPol, " +
                         "b.skdog1 AS SkDog, RTRIM(LTRIM(b.nsdoc)) + RTRIM(LTRIM(b.sdoc)) AS Document, " +
                         "b.ndoc AS Ndoc, b.polblank AS Polblank, b.id_doc AS IdDoc, " +
                         "b.seria AS Seria, b.number AS Number, b.vpolic AS Vpolic FROM #irobot rob " +
                         "JOIN(SELECT ph.ID, ph.vozrast, sf.family, sn.name, so.father, ph.enp, " +
                         "ph.id_pol, ph.skdog1, ph.nsdoc, ph.sdoc, ph.ndoc, " +
                         "ph.polblank, ph.id_doc, ph.seria, ph.number, ph.vpolic FROM polises_history ph " +
                         "JOIN spr_fam sf ON ph.id_fam = sf.id_fam " +
                         "JOIN spr_nam sn ON ph.id_nam = sn.id_nam " +
                         "JOIN spr_otc so ON ph.id_otc = so.id_otc " +
                         "WHERE ph.id_sost IN(164, 512)) AS b " +
                         "ON YEAR(b.vozrast) = YEAR(rob.dr) AND " +
                         "MONTH(b.vozrast) = MONTH(rob.dr) AND " +
                         "DAY(b.vozrast) = DAY(rob.dr) AND " +
                         "b.nsdoc = rob.docser AND " +
                         "b.ndoc = rob.docnum " +
                         "WHERE b.ID IN(SELECT * FROM dbo.Get_polises_from_history(rob.dateout) gpfh)";

            using (var sqlGrid = await _sqlConnection.QueryMultipleAsync(sql, commandTimeout: 0))
            {
                var searchName = await sqlGrid.ReadAsync<ErzResult>();
                var searchDocument = await sqlGrid.ReadAsync<ErzResult>();

                _sqlConnection.Close();

                return PersonLink(personItems.ToList(), searchName.Union(searchDocument).ToList());
            }
        }

        private IEnumerable<Person> PersonLink(List<Person> persons, List<ErzResult> items)
        {
            var dictionary = persons.ToDictionary(p => p.Id);
            foreach (var item in items.GroupBy(p => new { p.Fam, p.Im, p.Ot, p.Dr })
                .Select(p => new { key = p.Key, Items = p.ToList(), Count = p.DistinctBy(i => i.Enp).Count() }))
            {
                if (item.Count > 1)
                    continue;

                foreach (var source in item.Items)
                {
                    Person person;
                    if (dictionary.TryGetValue(source.Id, out person))
                    {
                        if (person != null)
                        {
                            person.PersonType = PersonSearchType.Erz;
                            person.ErzResult = source;
                            dictionary[source.Id] = person;
                        }
                    }
                }
            }

            return dictionary.Select(p => p.Value);
        }
    }
}