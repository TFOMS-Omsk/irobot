﻿using System;
using System.IO;
using System.IO.Compression;
using System.IO.FileSystem;
using System.Linq;

namespace Lib.Worker.Job
{
    public class FileZip : IFileZip
    {
        private Paths Paths => RobotConfig.Current.Settings.Paths;

        /// <summary>
        /// Распаковывает файлы из архива и преобразует файлы в поток сообщения
        /// </summary>
        /// <param name="file"></param>
        public MessageState Unpack(FileChange file)
        {
            string messageId = Guid.NewGuid().ToString("D");
            string path = file.Path.Replace("UNC", "\\");

            string unpackDirectory = Path.Combine(Paths.Extract, messageId);
            if (!Directory.Exists(unpackDirectory))
            {
                Directory.CreateDirectory(unpackDirectory);
            }

            if (Directory.GetFiles(unpackDirectory).Any())
            {
                foreach (var item in Directory.GetFiles(unpackDirectory))
                {
                    File.Delete(item);
                }
            }

            ZipFile.ExtractToDirectory(path, unpackDirectory);

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            string[] filesUnpack = Directory.GetFiles(unpackDirectory);

            string hUnpack = filesUnpack.FirstOrDefault(p =>
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(p);
                return fileNameWithoutExtension != null && !fileNameWithoutExtension.StartsWith("L");
            });

            string lUnpack = filesUnpack.FirstOrDefault(p =>
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(p);
                return fileNameWithoutExtension != null && fileNameWithoutExtension.StartsWith("L");
            });

            return new MessageState
            {
                MessageId = messageId,
                PathH = hUnpack,
                PathL = lUnpack
            };
        }
    }
}