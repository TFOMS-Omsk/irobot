﻿namespace Lib.Worker.Job
{
    public class MessageState
    {
        public string MessageId { get; set; }

        public string PathL { get; set; }

        public string PathH { get; set; }
    }
}