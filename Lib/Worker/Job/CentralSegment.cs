﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class CentralSegment : ICentralSegment
    {
        private string _messageId;

        private List<Person> _items;

        private string _messagePath;

        public ICentralSegment MessageId(string messageId)
        {
            _messageId = messageId;
            return this;
        }

        public ICentralSegment Persons(List<Person> items)
        {
            _items = items;
            return this;
        }

        public ICentralSegment ReadPack(string messagePath)
        {
            _messagePath = messagePath;
            return this;
        }

        public async Task<ICentralSegment> ShapeBuildAsync()
        {
            if (_messageId == null)
                throw new ArgumentNullException(nameof(_messageId));

            if (_items == null)
                throw new ArgumentNullException(nameof(_items));

            return await Task.Run(async () => await Processing());
        }

        private async Task<ICentralSegment> Processing()
        {
            await WriteMessageZp1(_items);
            return this;
        }

        public async Task<string> WriteMessageZp1(List<Person> items)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            if (items.Count == 0)
                throw new Exception();

            var date = DateTimeOffset.Now.ToString("yyyy-MM-ddThh:mm:sszzzz");

            var message = new XElement("UPRMessageBatch",
                new XElement("BHS",
                    new XElement("BHS.1", "|"),
                    new XElement("BHS.2", "^~\\&"),
                    new XElement("BHS.3",
                        new XElement("HD.1", "СРЗ 55")),
                    new XElement("BHS.4",
                        new XElement("HD.1", "55"),
                        new XElement("HD.2", "1.2.643.2.40.3.3.1.0"),
                        new XElement("HD.3", "ISO")),
                    new XElement("BHS.5",
                        new XElement("HD.1", "ЦК ЕРП")),
                    new XElement("BHS.6",
                        new XElement("HD.1", "00"),
                        new XElement("HD.2", "1.2.643.2.40.3.3.1.0"),
                        new XElement("HD.3", "ISO")),
                    new XElement("BHS.7", date),
                    new XElement("BHS.11", _messageId)),
                new XAttribute(XNamespace.Xmlns + "xsi", (XNamespace)"http://www.w3.org/2001/XMLSchema-instance"),
                new XAttribute(XNamespace.Xmlns + "rtc", (XNamespace)"http://www.rintech.ru"),
                new XAttribute(XNamespace.Xmlns + "xsd", (XNamespace)"http://www.w3.org/2001/XMLSchema"),
                new XAttribute("rtv", "urn:hl7-org:v2xml"));

            var messageBatch = new XDocument(
                new XDeclaration("1.0", "windows-1251", null));

            int count = 0;
            foreach (var item in items)
            {
                message.Add(new XElement("QBP_ZP1",
                    new XElement("MSH",
                        new XElement("MSH.1", "|"),
                        new XElement("MSH.2", "^~\\&"),
                        new XElement("MSH.3",
                            new XElement("HD.1", "СРЗ 55")),
                        new XElement("MSH.4",
                            new XElement("HD.1", "55"),
                            new XElement("HD.2", "1.2.643.2.40.3.3.1.0"),
                            new XElement("HD.3", "ISO")),
                        new XElement("MSH.5",
                            new XElement("HD.1", "ЦК ЕРП")),
                        new XElement("MSH.6",
                            new XElement("HD.1", "00"),
                            new XElement("HD.2", "1.2.643.2.40.3.3.1.0"),
                            new XElement("HD.3", "ISO")),
                        new XElement("MSH.7", date),
                        new XElement("MSH.9",
                            new XElement("MSG.1", "QBP"),
                            new XElement("MSG.2", "ZP1"),
                            new XElement("MSG.3", "QBP_ZP1")),
                        new XElement("MSH.10", item.Id),
                        new XElement("MSH.11",
                            new XElement("PT.1", "P")),
                        new XElement("MSH.12",
                            new XElement("VID.1", "2.6")),
                        new XElement("MSH.15", "AL"),
                        new XElement("MSH.16", "AL")),
                    new XElement("QPD",
                        new XElement("QPD.1",
                            new XElement("CWE.1", "СП"),
                            new XElement("CWE.3", "1.2.643.2.40.1.9")),
                        new XElement("QPD.3", "У"),
                        new XElement("QPD.5",
                            new XElement("CX.1", $"{item.DocSer} № {item.DocNum}"),
                            new XElement("CX.5", item.Doctype)),
                        new XElement("QPD.6",
                            new XElement("XPN.1",
                                new XElement("FN.1", item.Fam)),
                            new XElement("XPN.2", item.Im),
                            new XElement("XPN.3", item.Ot),
                            new XElement("XPN.7", "L")),
                        new XElement("QPD.7", item.Dr.ToString("yyyy-MM-dd")),
                        new XElement("QPD.8", item.W))));

                count++;
            }

            var resultQbpZp1 = string.Join("", message.Elements("QBP_ZP1")
                .ToList().Select(p => p.ToString().AsDeleteSymbols()).ToList());

            byte[] qbpZp1 = Encoding.Default.GetBytes(resultQbpZp1);

            var crc32 = new Crc32();
            byte[] hash = crc32.ComputeHash(qbpZp1, 0, qbpZp1.Length);
            var strHash = string.Join("", hash.Select(p => p.ToString("x2")));

            message.Add(new XElement("BTS",
                new XElement("BTS.1", count),
                new XElement("BTS.3", strHash)));

            messageBatch.Add(message);
            var pathSegment = Path.Combine(RobotConfig.Current.Settings.Paths.SegmentInput, $"52000-{_messageId}.uprmes");

            var resultFile = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\r\n" + messageBatch.ToString().Replace("rtv", "xmlns");
            using (var writer = new StreamWriter(new FileStream(pathSegment,
                    FileMode.CreateNew, FileAccess.Write), Encoding.GetEncoding(1251)))
            {
                await writer.WriteAsync(resultFile.AsDeleteSymbols());
            }

            return _messageId;
        }

        public async Task<IEnumerable<InsuranceGroupEnp>> ReadMessageAsync()
        {
            using (var reader = new StreamReader(_messagePath, Encoding.GetEncoding(1251)))
            {
                string messageReader = await reader.ReadToEndAsync();
                const string xmlns = "urn:hl7-org:v2xml";
                return (from elem in XDocument.Parse(messageReader)
                         .Elements(XName.Get("UPRMessageBatch", xmlns))
                         .Elements(XName.Get("RSP_ZK1", xmlns))
                        let msaTag = elem.Element(XName.Get("MSA", xmlns))
                        let msaNumber = msaTag != null ? msaTag.Element(XName.Get("MSA.2", xmlns))?.Value ?? "-1" : "-1"

                        let rspQuery = elem.Element(XName.Get("RSP_ZK1.QUERY_RESPONSE", xmlns))

                        //Enp
                        let enp = rspQuery != null ?
                                (from elemEnp in rspQuery.Elements(XName.Get("PID", xmlns)).Elements(XName.Get("PID.3", xmlns))
                                 select elemEnp.Element(XName.Get("CX.1", xmlns))?.Value ?? string.Empty).ToList() :
                                new List<string>()

                        //Страховки
                        let insurance = rspQuery != null ?
                                   (from item in rspQuery.Elements(XName.Get("IN1", xmlns))
                                    let num = item.Element(XName.Get("IN1.1", xmlns))?.Value ?? string.Empty
                                    let ogrnSource = item.Element(XName.Get("IN1.3", xmlns))
                                    let ogrn = ogrnSource == null ? string.Empty : ogrnSource.Element(XName.Get("CX.1", xmlns))?.Value ?? string.Empty
                                    let dateIn = item.Element(XName.Get("IN1.12", xmlns))?.Value ?? string.Empty
                                    let dateOut = item.Element(XName.Get("IN1.13", xmlns))?.Value ?? string.Empty
                                    let code = item.Element(XName.Get("IN1.15", xmlns))?.Value ?? string.Empty
                                    let typePolis = item.Element(XName.Get("IN1.35", xmlns))?.Value ?? string.Empty
                                    let polis = item.Element(XName.Get("IN1.36", xmlns))?.Value ?? string.Empty
                                    select new Insurance
                                    {
                                        Num = num,
                                        Ogrn = ogrn,
                                        DateIn = dateIn,
                                        DateOut = dateOut,
                                        CodeOkato = code,
                                        TypePolis = typePolis,
                                        Polis = polis
                                    }).ToList() : new List<Insurance>()
                        where rspQuery != null
                        select new InsuranceGroupEnp(insurance, enp, int.Parse(msaNumber))).ToList();
            }
        }
    }
}