﻿using System.Diagnostics;

namespace Lib.Worker.Job
{
    public class JobLog : IJobLog
    {
        private readonly EventLog _eventLog;

        private const string EventSourceName = "RobotService";

        private const string EventLogName = "RobotServiceLog";

        private readonly object _state = new object();

        public JobLog()
        {
            lock (_state)
            {
                _eventLog = new EventLog();

                if (!EventLog.SourceExists(EventSourceName))
                {
                    EventLog.CreateEventSource(EventSourceName, EventLogName);
                }

                _eventLog.Source = EventSourceName;
                _eventLog.Log = EventLogName;
            }
        }

        public void WriteLog(string message, EventLogEntryType eventLog = EventLogEntryType.Information)
        {
            lock (_state)
            {
                _eventLog.WriteEntry(message, eventLog);
            }
        }

        public void Dispose()
        {
            _eventLog?.Dispose();
        }
    }
}