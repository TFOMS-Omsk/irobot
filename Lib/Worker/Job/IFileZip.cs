﻿using System.IO.FileSystem;

namespace Lib.Worker.Job
{
    public interface IFileZip
    {
        MessageState Unpack(FileChange file);
    }
}