﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class PackXml : IPackXml
    {
        public async Task<IEnumerable<Person>> GetPersons(MessageState state)
        {
            using (var readerL = new StreamReader(state.PathL, Encoding.GetEncoding(1251)))
            using (var readerH = new StreamReader(state.PathH, Encoding.GetEncoding(1251)))
            {
                var contentL = await readerL.ReadToEndAsync();
                var contentH = await readerH.ReadToEndAsync();

                int index = 0;
                var persons = (from elem in XDocument.Parse(contentL).Elements("PERS_LIST").Elements("PERS")
                               let idPac = elem.Element("ID_PAC")
                               let fam = elem.Element("FAM")
                               let im = elem.Element("IM")
                               let ot = elem.Element("OT")
                               let w = elem.Element("W")
                               let dr = elem.Element("DR")
                               let doctype = elem.Element("DOCTYPE")
                               let docser = elem.Element("DOCSER")
                               let docnum = elem.Element("DOCNUM")
                               let snils = elem.Element("SNILS")
                               select new Person
                               {
                                   Id = ++index,
                                   IdPac = idPac?.Value.Trim() ?? string.Empty,
                                   Fam = fam?.Value.Trim().ToUpper().Replace("Ё", "Е") ?? string.Empty,
                                   Im = im?.Value.Trim().ToUpper().Replace("Ё", "Е") ?? string.Empty,
                                   Ot = ot?.Value.Trim().ToUpper().Replace("Ё", "Е") ?? string.Empty,
                                   W = w?.Value.Trim() ?? string.Empty,
                                   Dr = dr?.Value.AsDateTime() ?? new DateTime(3000, 1, 1),
                                   Doctype = doctype?.Value ?? string.Empty,
                                   DocSer = docser?.Value ?? string.Empty,
                                   DocNum = docnum?.Value ?? string.Empty,
                                   Snils = snils?.Value ?? string.Empty
                               }).ToDictionary(p => p.IdPac);

                var usls = from elem in XDocument.Parse(contentH)
                           .Elements("ZL_LIST").Elements("ZAP")
                           let pacient = elem.Element("PACIENT")
                           let idPac = pacient.Element("ID_PAC")?.Value ?? string.Empty
                           let dateOut = (from element in elem.Elements("SLUCH")
                                          select element.Element("DATE_2")?.Value ?? string.Empty).FirstOrDefault()
                                          select new
                                          {
                                              idPac,
                                              dateOut
                                          };

                foreach (var usl in usls)
                {
                    Person person;
                    if (persons.TryGetValue(usl.idPac, out person))
                    {
                        person.DateOut = usl.dateOut.AsDateTime();
                        persons[usl.idPac] = person;
                    }
                }

                return persons.Select(p => p.Value);
            }
        }
    }
}