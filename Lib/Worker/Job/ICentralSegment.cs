﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public interface ICentralSegment
    {
        /// <summary>
        /// Идентификатор сообщения
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        ICentralSegment MessageId(string messageId);

        /// <summary>
        /// Пациенты
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        ICentralSegment Persons(List<Person> items);
        
        ICentralSegment ReadPack(string messagePath);

        /// <summary>
        /// Формирование файлов для центрального сегмента и отправка
        /// </summary>
        /// <returns></returns>
        Task<ICentralSegment> ShapeBuildAsync();

        /// <summary>
        /// Чтение сообщения из цс
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<InsuranceGroupEnp>> ReadMessageAsync();
    }
}