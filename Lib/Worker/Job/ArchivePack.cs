﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using JetBrains.Annotations;
using Lib.Ioc;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class ArchivePack : IArhivePack
    {
        /// <summary>
        /// Больницы исключения
        /// </summary>
        private string[] MoExceptions => RobotConfig.Current.Settings.MedException;

        private Paths Paths => RobotConfig.Current.Settings.Paths;

        private JobMessage _job;

        private string _contentH;

        private string _contentL;

        public IArhivePack AddJob([NotNull] JobMessage job)
        {
            _job = job;
            return this;
        }

        private async Task Init()
        {
            using (var readingH = new StreamReader(_job.State.PathH, Encoding.GetEncoding(1251)))
            using (var readingL = new StreamReader(_job.State.PathL, Encoding.GetEncoding(1251)))
            {
                _contentH = await readingH.ReadToEndAsync();
                _contentL = await readingL.ReadToEndAsync();
            }
        }

        public async Task Build()
        {
            if (_job == null)
            {
                throw new ArgumentException();
            }

            await Init();

            var itemsErz = _job.Persons.Where(p => p.PersonType == PersonSearchType.Erz);
            var itemsNone = _job.Persons.Where(p => p.PersonType == PersonSearchType.None);
            var itemsSegment = _job.Persons.Where(p => p.PersonType == PersonSearchType.Segment);

            string name = Path.GetFileNameWithoutExtension(_job.Name);
            foreach (var item in itemsErz.GroupBy(p => p.ErzResult.SkDog))
            {
                await CreateArchive(item.ToList(), name, item.Key.ToString());
            }

            var enumerableSegment = itemsSegment as Person[] ?? itemsSegment.ToArray();
            if (enumerableSegment.Any())
            {
                await CreateArchive(enumerableSegment.ToList(), name, "55");
            }

            var enumerableNone = itemsNone as Person[] ?? itemsNone.ToArray();
            if (enumerableNone.Any())
            {
                await CreateArchive(enumerableNone.ToList(), name, "NI");
            }

            //Чистим каталоги
            string pathSourceFile = Path.Combine(Paths.Extract, _job.State.MessageId);
            if (Directory.Exists(pathSourceFile))
            {
                foreach (var file in Directory.GetFiles(pathSourceFile))
                {
                    File.Delete(file);
                }

                Directory.Delete(pathSourceFile);
            }

            string[] pathsSegment = Directory.GetFiles("\\" + Paths.SegmentOutput.Replace("UNC", string.Empty));
            foreach (var file in pathsSegment.Where(p =>
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(p);
                return !string.IsNullOrWhiteSpace(fileNameWithoutExtension) && StringComparer.Ordinal.Compare(fileNameWithoutExtension.Substring(6), _job.State.MessageId) == 0;
            }))
            {
                File.Delete(file);
            }
        }

        private async Task CreateArchive(List<Person> items, string name, string format)
        {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string directory = Path.Combine(dir, Guid.NewGuid().ToString());

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var pacients = await ArchiveH(items, name, format, directory);
            ArchiveL(pacients, name, format, directory);

            string archiveName = string.Empty;
            int count = Directory.GetFiles(Paths.Output).Count(p => Path.GetFileNameWithoutExtension(p)?.Contains(GetName(name, format)) ?? false);
            if (count > 0)
            {
                archiveName += GetName(name, format) + $"({count})" + ".zip";
            }
            else
            {
                archiveName += GetName(name, format) + ".zip";
            }

            ZipFile.CreateFromDirectory(directory, Path.Combine(Paths.Output, archiveName));
            foreach (var file in Directory.GetFiles(directory))
            {
                File.Delete(file);
            }

            Directory.Delete(directory);
        }

        private void ArchiveL(List<string> person, string name, string format, string directory)
        {
            XDocument document = XDocument.Parse(_contentL);
            var persZglv = document.Elements("PERS_LIST").Elements("ZGLV").FirstOrDefault();
            if (persZglv != null)
            {
                persZglv.Element("DATA")?.SetValue(DateTime.Now.ToString("yyyy-MM-dd"));
                persZglv.Element("FILENAME")?.SetValue("L" + GetName(name, format).Substring(1));
                persZglv.Element("FILENAME1")?.SetValue(GetName(name, format));
            }

            var zapDictionary = document.Elements("PERS_LIST")
                .Elements("PERS")
                .Where(p => person.Contains(p.Element("ID_PAC")?.Value))
                .Select(p => p).ToList();

            document.Elements("PERS_LIST").Elements("PERS").Remove();
            document.Element("PERS_LIST")?.Add(zapDictionary);

            string fileName = Path.Combine(directory, "L" + GetName(name, format).Substring(1)) + ".xml";
            File.WriteAllText(fileName, @"<?xml version=""1.0"" encoding=""windows-1251""?>" + document, Encoding.GetEncoding(1251));
        }

        /// <summary>
        /// Формирование файла H
        /// </summary>
        private async Task<List<string>> ArchiveH(List<Person> items, string name, string format, string directory)
        {
            IHandBook handBook = IocContainer.Default.Resolve<IHandBook>();
            var itemsSmo = await handBook.GetBookAsync();
            var smoHandBooks = itemsSmo as SmoHandBook[] ?? itemsSmo.ToArray();
            var dictionaryOgrn = smoHandBooks.ToList();

            ITarifUsl tarifUsl = IocContainer.Default.Resolve<ITarifUsl>();
            List<TarifModel> tarifResult = await tarifUsl.GetTarif();

            XDocument document = XDocument.Parse(_contentH);
            var zlList = document.Elements("ZL_LIST").Elements("ZGLV").FirstOrDefault();
            if (zlList != null)
            {
                zlList.Element("DATA")?.SetValue(DateTime.Now.ToString("yyyy-MM-dd"));
                zlList.Element("FILENAME")?.SetValue(GetName(name, format));
            }

            var zapDictionary = document.Elements("ZL_LIST").Elements("ZAP")
                .Select(p => new XNodeElement(p))
                .ToMultiDictionary(p => p.Element.Element("PACIENT")?.Element("ID_PAC")?.Value);

            document.Elements("ZL_LIST").Elements("ZAP").Remove();
            List<string> pacients = new List<string>();

            foreach (var element in items.ToMultiDictionary(p => p.IdPac))
            {
                var firstElement = element.Value.First();
                IReadOnlyCollection<XNodeElement> elem;
                if (zapDictionary.TryGetValue(firstElement.IdPac, out elem))
                {
                    var pacient = elem.First().Element.Element("PACIENT");
                    if (firstElement.PersonType == PersonSearchType.Erz)
                    {
                        Tuple<int, string, string> polis = SearchPolis(firstElement);
                        if (pacient != null)
                        {
                            if (polis.Item1 == 1)
                            {
                                pacient.Element("SPOLIS")?.SetValue(polis.Item3.Trim());
                            }
                            else
                            {
                                pacient.Element("SPOLIS")?.SetValue(string.Empty);
                            }

                            pacient.Element("VPOLIS")?.SetValue(polis.Item1);
                            pacient.Element("NPOLIS")?.SetValue(polis.Item2.Trim());
                        }
                    }
                    else if (firstElement.PersonType == PersonSearchType.Erz)
                    {
                        if (pacient != null)
                        {
                            int vpolis = 0;
                            if (firstElement.ErzResult.Vpolic != null)
                            {
                                switch (firstElement.ErzResult.Vpolic.Trim().ToUpper())
                                {
                                    case "С":
                                        vpolis = 1;
                                        break;
                                    case "В":
                                        vpolis = 2;
                                        break;
                                    case "П":
                                        vpolis = 3;
                                        break;
                                    case "Э":
                                        vpolis = 3;
                                        break;
                                }

                                pacient.Element("VPOLIS")?.SetValue(vpolis);
                                pacient.Element("NPOLIS")?.SetValue(firstElement.ErzResult.Number);

                                if (dictionaryOgrn.Exists(p => p.Ogrn == firstElement.ErzResult.Ogrn && p.Okato == firstElement.ErzResult.Okato))
                                {
                                    SmoHandBook handBookElement = dictionaryOgrn.FirstOrDefault(p => p.Ogrn == firstElement.ErzResult.Ogrn 
                                        && p.Okato == firstElement.ErzResult.Okato);

                                    string smoCode = handBookElement?.SmoCode ?? string.Empty;
                                    string ogrn = handBookElement?.Ogrn ?? string.Empty;

                                    pacient.Element("SMO_OK")?.SetValue(firstElement.ErzResult.Okato);
                                    pacient.Element("SMO")?.SetValue(smoCode);
                                    pacient.Element("SMO_OGRN")?.SetValue(ogrn);
                                }
                            }
                        }

                        //Больницы исключения
                        string codeMo = document.Element("ZL_LIST")?.Element("SCHET")?.Element("CODE_MO")?.Value ?? string.Empty;
                        if (MoExceptions.Contains(codeMo))
                        {
                            //Выбираем тариф и считаем сумму услуг
                            var elementSluch = elem.First().Element.Element("SLUCH");
                            var elementUsl = elementSluch?.Element("USL");

                            if (elementUsl != null)
                            {
                                var valueCodeUsl = elementUsl.Element("CODE_USL")?.Value ?? string.Empty;
                                double valueColUsl = double.TryParse(elementUsl.Element("KOL_USL")?.Value ?? string.Empty,
                                    NumberStyles.Any,
                                    CultureInfo.InvariantCulture,
                                    out valueColUsl) ? valueColUsl : 0.0;

                                var tarif = tarifResult.FirstOrDefault(p => StringComparer.Ordinal.Compare(valueCodeUsl, p.CodeUsl) == 0);
                                if (tarif != null)
                                {
                                    string valTarif = tarif.Tarif.ToString("N", CultureInfo.InvariantCulture).Replace(",", string.Empty);
                                    string valSumV = (tarif.Tarif * valueColUsl).ToString("N", CultureInfo.InvariantCulture).Replace(",", string.Empty);

                                    elementUsl.Element("TARIF")?.SetValue(valTarif);
                                    elementUsl.Element("SUMV_USL")?.SetValue(valSumV);

                                    elementSluch.Element("TARIF")?.SetValue(string.Empty);
                                    elementSluch.Element("SUMV")?.SetValue(valSumV);
                                }
                            }
                        }
                    }

                    pacients.Add(firstElement.IdPac);

                    var zap = zapDictionary[firstElement.IdPac].First();
                    zap.IsRead = true;

                    zapDictionary.Remove(firstElement.IdPac);
                    zapDictionary.Add(firstElement.IdPac, zap);
                }
            }

            //Добавляем записи
            document.Element("ZL_LIST")?.Add(zapDictionary.Where(p => p.Value.First().IsRead).Select(p => p.Value.First().Element));

            //Считаем сумму
            var sheet = document.Elements("ZL_LIST").Elements("SCHET");
            if (sheet != null)
            {
                var summvUsl = document.Elements("ZL_LIST").Elements("ZAP")
                    .Elements("SLUCH")
                    .Elements("USL")
                    .Elements("SUMV_USL")
                    .Where(p => !string.IsNullOrWhiteSpace(p.Value))
                    .Sum(p =>
                    {
                        double num = double.Parse(p.Value, CultureInfo.InvariantCulture);
                        return num;
                    });

                sheet.FirstOrDefault()?.Element("SUMMAV")?.SetValue(summvUsl.ToString("N", CultureInfo.InvariantCulture)
                    .Replace(",", string.Empty));
            }

            //Вставляем количество записей
            zlList?.Element("SD_Z")?.SetValue(zapDictionary.Count(p => p.Value.First().IsRead));

            string fileName = Path.Combine(directory, GetName(name, format)) + ".xml";
            File.WriteAllText(fileName, @"<?xml version=""1.0"" encoding=""windows-1251""?>" + document, Encoding.GetEncoding(1251));

            return pacients;
        }

        private string GetName(string name, string format)
        {
            string[] a = name.Split('_');
            a[0] = a[0].Substring(0, a[0].Length - 2);
            a[0] += format;

            return string.Join("_", a);
        }

        private Tuple<int, string, string> SearchPolis(Person item)
        {
            int vpolic = int.Parse(item.ErzResult.Vpolic);
            switch (vpolic)
            {
                case 1:
                    return new Tuple<int, string, string>(vpolic, item.ErzResult.Number, item.ErzResult.Seria);
                case 2:
                    return new Tuple<int, string, string>(vpolic, item.ErzResult.Polblank, string.Empty);
                case 3:
                    return new Tuple<int, string, string>(vpolic, item.ErzResult.Enp, string.Empty);
                case 4:
                    return new Tuple<int, string, string>(vpolic, item.ErzResult.Enp, string.Empty);
                case 5:
                    {
                        if (!string.IsNullOrWhiteSpace(item.ErzResult.Number) && !string.IsNullOrWhiteSpace(item.ErzResult.Seria))
                            return new Tuple<int, string, string>(5, $"{item.ErzResult.Number} {item.ErzResult.Seria}", string.Empty);

                        if (!string.IsNullOrWhiteSpace(item.ErzResult.Polblank))
                            return new Tuple<int, string, string>(5, item.ErzResult.Polblank, string.Empty);

                        return new Tuple<int, string, string>(5, item.ErzResult.Enp, string.Empty);
                    }
                default:
                    return new Tuple<int, string, string>(vpolic, item.ErzResult.Enp, string.Empty);
            }
        }
    }
}