﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public class TarifUsl : ITarifUsl
    {
        private DataInstances Instance => RobotConfig.Current.Settings.DataInstances;

        public async Task<List<TarifModel>> GetTarif()
        {
            var sqlSprav = new SqlConnection(Instance.Sprav);
            var sqlTarif = new SqlConnection(Instance.Tarif);

            //Тарифы
            IEnumerable<Tarif> tarifs = await sqlTarif.QueryAsync<Tarif>("SELECT idMIAC_SprCodeUsl AS IdCodeUsl, " +
                                                                         "tarif AS TarifSum, DateBegin AS DateBegin FROM TarifSP");
            var enumerableTarif = tarifs as Tarif[] ?? tarifs.ToArray();

            //Коды услуг
            var codeService = await sqlSprav.QueryAsync<SprCodeUsl>("SELECT id AS Id, code_usl AS CodeUsl " +
                                                                    $"FROM MIAC_SprCodeUsl WHERE id IN({string.Join(",", enumerableTarif.Select(p => p.IdCodeUsl))})");
            var enumerableCodeService = codeService as SprCodeUsl[] ?? codeService.ToArray();

            var results = new List<TarifModel>();
            foreach (var tarif in enumerableTarif)
            {
                var service = enumerableCodeService.First(p => p.Id == tarif.IdCodeUsl);

                results.Add(new TarifModel
                {
                    CodeUsl = service.CodeUsl,
                    DateBegin = tarif.DateBegin,
                    Tarif = tarif.TarifSum
                });
            }

            return results;
        }
    }
}