﻿using System;
using System.Diagnostics;

namespace Lib.Worker.Job
{
    public interface IJobLog : IDisposable
    {
        void WriteLog(string message, EventLogEntryType eventLog = EventLogEntryType.Information);
    }
}