﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lib.Worker.Model;

namespace Lib.Worker.Job
{
    public interface IErzJob
    {
        Task<IEnumerable<Person>> GetErzList(IEnumerable<Person> persons);
    }
}