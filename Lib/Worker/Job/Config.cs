﻿using System;
using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace Lib.Worker.Job
{
    public class Config : IConfig
    {
        public async Task<WorkerSettings> GetSettings([NotNull] string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("appsettings null or empty");
            }

            using (var read = new StreamReader(new FileStream(path, FileMode.Open, FileAccess.Read)))
            {
                var content = await read.ReadToEndAsync();
                try
                {
                    return JsonConvert.DeserializeObject<WorkerSettings>(content);
                }
                catch (JsonException)
                {
                    return null;
                }
            }
        }
    }
}