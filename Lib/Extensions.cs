﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lib
{
    public static class Extensions
    {
        public static DateTime AsDateTime(this string time)
        {
            if (string.IsNullOrWhiteSpace(time))
                return DateTime.Now;

            return DateTime.ParseExact(time, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        public static string AsNum(this int numeric)
        {
            if (numeric > 0 && numeric < 10)
            {
                return $"0{numeric}";
            }

            return numeric.ToString();
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> hashSet = new HashSet<TKey>();
            return source.Where(item => hashSet.Add(keySelector(item)));
        }

        public static string AsDeleteSymbols(this string value)
        {
            return Regex.Replace(value.Replace("\r\n", ""), @"\s{2,}", "");
        }

        public static MultiValueDictionary<K, T> ToMultiDictionary<T, K>(this IEnumerable<T> source, Func<T, K> keySelector)
        {
            var multi = new MultiValueDictionary<K, T>();
            foreach (var item in source)
            {
                multi.Add(keySelector(item), item);
            }

            return multi;
        }
    }
}