﻿namespace Lib.Ioc
{
    public class SingleStrategy<T> : IStrategy where T : class
    {
        public readonly T Instance;

        public SingleStrategy(T instance)
        {
            Instance = instance;
        }

        public object GetValue()
        {
            return Instance;
        }
    }
}