﻿using System;

namespace Lib.Ioc
{
    public class PerRequestStrategy<T> : IStrategy where T : class
    {
        private readonly Func<T> _func;

        public PerRequestStrategy(Func<T> func)
        {
            _func = func;
        }

        public object GetValue()
        {
            if (_func == null)
            {
                throw new ArgumentNullException(nameof(_func));
            }

            return _func();
        }
    }
}