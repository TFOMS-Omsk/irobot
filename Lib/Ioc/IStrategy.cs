﻿namespace Lib.Ioc
{
    public interface IStrategy
    {
        object GetValue();
    }
}