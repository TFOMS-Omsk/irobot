﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Lib.Ioc
{
    public sealed class IocContainer
    {
        private static readonly Lazy<IocContainer> Lazy = new Lazy<IocContainer>(() => new IocContainer(), LazyThreadSafetyMode.ExecutionAndPublication);

        private static readonly Dictionary<Type, IStrategy> Dictionary = new Dictionary<Type, IStrategy>();

        private IocContainer()
        {
        }

        public static IocContainer Default => Lazy.Value;
        
        public IocContainer Single<TKey, TValue>() where TValue : class, new()
        {
            Dictionary[typeof(TKey)] = new SingleStrategy<TValue>(new TValue());

            return this;
        }

        public IocContainer Single<TKey, TValue>(TValue instance) where TValue : class
        {
            Dictionary[typeof(TKey)] = new SingleStrategy<TValue>(instance);
            return this;
        }

        public IocContainer PerRequest<TKey, TValue>() where TValue : class, new()
        {
            Dictionary[typeof(TKey)] = new PerRequestStrategy<TValue>(() => new TValue());

            return this;
        }

        public T Resolve<T>()
        {
            IStrategy result;
            return Dictionary.TryGetValue(typeof(T), out result) ? (T)result.GetValue() : default(T);
        }

        public IocContainer Clear()
        {
            Dictionary.Clear();

            return this;
        }
    }
}